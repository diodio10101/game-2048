package com.example.a2048.app

import android.app.Application
import com.example.a2048.data.sourse.MySher

class App:Application() {
    override fun onCreate() {
        super.onCreate()
        MySher.init(this)
    }
}