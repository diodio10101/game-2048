package com.example.a2048.domain

interface AppRepository {
    fun setMat(score:Int)
    fun getMat():ArrayList<ArrayList<Int>>
    fun moveLeft():Boolean
    fun moveRight():Boolean
    fun moveUp():Boolean
    fun moveDown():Boolean
    fun getScore():Int
    fun refreshMatrix()
    fun cleatShared()
}