package com.example.a2048.domain

import com.example.a2048.data.sourse.MySher
import com.example.group1.utils.myLog
import kotlin.random.Random

class AppRepositoryImpl:AppRepository {
    private val MATRIX_SIZE=4
    private var addElementAmount=2
    private var score=0
    private var matrix= MySher.getMat()
    init {
        var  count=0
        matrix.forEach {
            it.forEach {
                count+=it
            }
        }
        if (count==0){
        addNewElement()
        addNewElement()

        }else{
            score=MySher.getScore()
        }
    }

    override fun setMat(score: Int) {
        MySher.setMat(matrix)
        MySher.setScore(score)
    }

    override fun getMat(): ArrayList<ArrayList<Int>> =matrix

    override fun moveLeft():Boolean {
        var scoreLeft=0
        val newMatrix = getNew()
        var isMove=false
        var index: Int
        for (i in 0..<matrix.size){
            index=0
            for (j in 0..<matrix[i].size){
                if (matrix[i][j]!=0){
                    if (newMatrix[i][index]==0){
                        newMatrix[i][index]= matrix[i][j]
                    }else if (newMatrix[i][index]== matrix[i][j]){
                        newMatrix[i][index]*=2
                        scoreLeft+=newMatrix[i][index]
                        index++
                        isMove=true
                    }else{
                        newMatrix[i][++index] = matrix[i][j]
                    }
                }
            }
        }
        for (i in 0..<4){
            for (j in 0..<4){
                if (newMatrix[i][j]!=matrix[i][j]){
                    isMove=true
                }
            }
        }
        score+=scoreLeft
        matrix = newMatrix
        if (isMove)addNewElement()
        return checkLose()

    }

    override fun moveRight():Boolean {
        var scoreRight=0
        val newMatrix = getNew()
        var index: Int
        var isMove=false
        for (i in 0..<matrix.size){
            index=3
            for (j in matrix[i].size-1 downTo 0){
                if (matrix[i][j]!=0){
                    if (newMatrix[i][index]==0){
                        newMatrix[i][index]= matrix[i][j]
                    }else if (newMatrix[i][index]== matrix[i][j]){
                        newMatrix[i][index]*=2
                        scoreRight+=newMatrix[i][index]
                        index--
                        isMove=true
                    }else{
                        newMatrix[i][--index] = matrix[i][j]
                    }
                }
            }
        }
        for (i in 0..<4){
            for (j in 0..<4){
                if (newMatrix[i][j]!=matrix[i][j]){
                    isMove=true
                }
            }
        }
        score+=scoreRight
        matrix = newMatrix
        if (isMove)addNewElement()
        return checkLose()
    }

    private fun addNewElement() {
        val empty = ArrayList<Pair<Int, Int>>()
        for (i in matrix.indices) {
            for (j in matrix[i].indices) {
                if (matrix[i][j] == 0) empty.add(Pair(i, j))
            }
        }

        if (empty.isEmpty()) return
        val randomIndex = Random.nextInt(0, empty.size)
        val findPairByRandomIndex = empty[randomIndex]
        matrix[findPairByRandomIndex.first][findPairByRandomIndex.second] = addElementAmount
//        score+= addElementAmount
    }

    override fun moveUp():Boolean {
        var scoreUp=0
        val newMatrix = getNew()
        var index: Int
        var isMove=false
        for (j in 0..<matrix.size){
            index=0
            for (i in 0..<matrix.size){
                if (matrix[i][j]!=0){
                    if (newMatrix[index][j]==0)newMatrix[index][j]= matrix[i][j]
                    else if(newMatrix[index][j]== matrix[i][j]){
                        newMatrix[index][j]*=2
                        scoreUp+=newMatrix[index][j]
                        index++
                        isMove=true
                    }else{
                        newMatrix[++index][j]= matrix[i][j]
                    }
                }
            }
        }
        for (i in 0..<4){
            for (j in 0..<4){
                if (newMatrix[i][j]!=matrix[i][j]){
                    isMove=true
                }
            }
        }
        score+=scoreUp
        matrix = newMatrix
        if (isMove)addNewElement()
        return checkLose()
    }

    override fun moveDown():Boolean {
        var scoreDown=0
        val newMatrix = getNew()
        var index: Int
        var isMove=false
        for (j in 0..<matrix.size){
            index=3
            for (i in matrix.size-1 downTo 0){
                if (matrix[i][j]!=0){
                    if (newMatrix[index][j]==0)newMatrix[index][j]= matrix[i][j]
                    else if(newMatrix[index][j]== matrix[i][j]){
                        newMatrix[index][j]*=2
                        scoreDown+=newMatrix[index][j]
                        index--
                        isMove=true
                    }else{
                        newMatrix[--index][j]= matrix[i][j]
                    }
                }
            }
        }
        for (i in 0..<4){
            for (j in 0..<4){
                if (newMatrix[i][j]!=matrix[i][j]){
                    isMove=true
                }
            }
        }
        score+=scoreDown
        matrix = newMatrix
        if (isMove)addNewElement()
        return checkLose()
    }

    override fun getScore(): Int = score

    fun getNew():ArrayList<ArrayList<Int>>{
        var mat= arrayListOf(
            arrayListOf(0,0,0,0),
            arrayListOf(0,0,0,0),
            arrayListOf(0,0,0,0),
            arrayListOf(0,0,0,0)
        )
        return mat
    }
    override fun refreshMatrix(){
        matrix= arrayListOf(
            arrayListOf(0,0,0,0),
            arrayListOf(0,0,0,0),
            arrayListOf(0,0,0,0),
            arrayListOf(0,0,0,0)
        )
        addNewElement()
        addNewElement()
        score=0
    }

    override fun cleatShared() {
        "salom".myLog()
        MySher.setMat(arrayListOf(
            arrayListOf(0,0,0,0),
            arrayListOf(0,0,0,0),
            arrayListOf(0,0,0,0),
            arrayListOf(0,0,0,0)
        ))
    }

    fun checkLose(): Boolean {
        for (i in matrix.indices) {
            for (j in matrix[i].indices) {
                if (matrix[i][j] == 0) {
                    return false
                }
            }
        }

        for (i in matrix.indices) {
            for (j in matrix[i].indices) {
                if (j < 4 - 1 && matrix[i][j] == matrix[i][j + 1]) {
                    return false
                }
                if (i < 4 - 1 && matrix[i][j] == matrix[i + 1][j]) {
                    return false
                }
            }
        }

        return true
    }
}