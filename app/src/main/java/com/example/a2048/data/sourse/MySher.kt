package com.example.a2048.data.sourse

import android.content.Context
import android.content.SharedPreferences
import kotlin.collections.ArrayList

object MySher {
    private lateinit var sharedPreferences:SharedPreferences
    fun init(context: Context){
        sharedPreferences=context.getSharedPreferences("Mydata", Context.MODE_PRIVATE)
    }
    fun setHighScore(score:Int){
        sharedPreferences.edit().putInt("score",score).apply()
    }
    fun getHighScore():Int= sharedPreferences.getInt("score",0)

    fun setMat(mat:ArrayList<ArrayList<Int>>){
        sharedPreferences.edit().putInt("sizeI",mat.size).apply()
        sharedPreferences.edit().putInt("sizeJ",mat[0].size).apply()
        for (i in 0 until  mat.size){
            for (j in 0 until mat[i].size){
                sharedPreferences.edit().putInt("mat$i/$j",mat[i][j]).apply()
            }
        }
    }
    fun getMat():ArrayList<ArrayList<Int>>{
        val mat=ArrayList<ArrayList<Int>>()
        val sizeI= sharedPreferences.getInt("sizeI",4)
        val sizeJ= sharedPreferences.getInt("sizeJ",4)
        for (i in 0 until sizeI){
            val list=ArrayList<Int>()
            for (j in 0 until sizeJ){
                list.add( sharedPreferences.getInt("mat$i/$j",0))
            }
            mat.add(list)
        }
        return mat
    }

    fun setScore(score:Int){
        sharedPreferences.edit().putInt("currentScore",score).apply()
    }

    fun getScore(): Int {
        return sharedPreferences.getInt("currentScore",0)
    }
}