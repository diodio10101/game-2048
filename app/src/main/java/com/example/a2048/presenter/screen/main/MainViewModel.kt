package com.example.a2048.presenter.screen.main

import androidx.lifecycle.ViewModel

class MainViewModel:ViewModel(),MainContract.ViewModel {
    var load:((ArrayList<ArrayList<Int>>,Boolean)->Unit)?=null
    var showScore:((Int)->Unit)?=null
    var isFinish=false
    private val model:MainContract.Model=MainModel()
    override fun setMat() {
        model.setMat(model.getScore())
    }


    override fun loadMat() {
        load?.invoke(model.getMat(),isFinish)
        if (isFinish==true){
            model.refresh()
        }
    }

    override fun moveRight() {
        isFinish=model.moveRight()
    }

    override fun moveLeft() {
        isFinish=model.moveLeft()
    }

    override fun moveDown() {
        isFinish=model.moveDown()
    }

    override fun moveUp() {
       isFinish=model.moveUp()
    }

    override fun setScore() {
        if (!isFinish){
            showScore?.invoke(model.getScore())
        }
    }

    override fun refresh() {
        isFinish=false
        load?.invoke(model.refresh(),isFinish)
    }

    override fun cleatShared() {
        model.cleatShared()
    }


}