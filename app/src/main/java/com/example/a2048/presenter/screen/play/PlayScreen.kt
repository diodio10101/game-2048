package com.example.a2048.presenter.screen.play

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.a20048.createDialog
import com.example.a20048.setStatusBar
import com.example.a2048.R
import com.example.a2048.databinding.ScreenPlayBinding

class PlayScreen:Fragment(R.layout.screen_play) {
    private var _binding:ScreenPlayBinding?=null
    private val binding by lazy { _binding!! }
    private val navController:NavController by lazy { findNavController() }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding=ScreenPlayBinding.inflate(inflater, container, false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        init()
        requireActivity().setStatusBar(binding.spase)
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
               openExitDialog()
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }

    fun init(){
        binding.apply {
            play.setOnClickListener {
                navController.navigate(R.id.action_playScreen_to_mainScreen)
            }
            info.setOnClickListener {
                navController.navigate(R.id.action_playScreen_to_infoScreen)
            }
//            exit.setOnClickListener {
//                openExitDialog()
//            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding=null
    }

    override fun onStart() {
        super.onStart()
        anim()
    }
    private fun anim(){

        binding.apply {


            root.post {
                val infoX=info.x

                gameIcon.scaleX=0f
                gameIcon.scaleY=0f
                gameIcon.animate()
                    .scaleY(1f)
                    .scaleX(1f)
                    .setDuration(500)
                    .start()
                info.x=infoX+info.width
                info.animate()
                    .x(infoX)
                    .setDuration(500)
                    .start()
                play.scaleX=0f
                play.scaleY=0f
                play.animate()
                    .scaleY(1f)
                    .scaleX(1f)
                    .setDuration(500)
                    .start()

//                val cocaY=coca.y
//                coca.y = -coca.width.toFloat()
//                coca.animate().y(cocaY).setDuration(500).start()
            }
        }
    }
    private fun openExitDialog(){
        AlertDialog.Builder(requireActivity())
            .setTitle("Exit")
            .setMessage("Are you sure you want to go out?")
            .setPositiveButton("no", { dialog, which ->

            })
            .setNegativeButton("yes", { dialog, which ->
                requireActivity().finish()
            })
            .create()
            .show()
    }
}