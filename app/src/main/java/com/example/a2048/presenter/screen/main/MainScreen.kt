package com.example.a2048.presenter.screen.main

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.graphics.Color
import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.animation.addListener
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import androidx.navigation.fragment.findNavController
import com.example.a20048.createDialog
import com.example.a20048.dpToPx
import com.example.a20048.setStatusBar
import com.example.a2048.MyTouchListener
import com.example.a2048.R
import com.example.a2048.data.sourse.MySher
import com.example.a2048.databinding.ScreenMainBinding
import com.example.a2048.presenter.screen.dialog.FinishDialog
import com.example.group1.utils.myLog
import com.example.group1.utils.popBackStack

class MainScreen : Fragment(R.layout.screen_main) {
    private var _binding: ScreenMainBinding? = null
    private val binding by lazy { _binding!! }
    private var buttons = ArrayList<AppCompatTextView>(16)
    private val viewModel = MainViewModel()
    private val shar = MySher
    private var oldScore = -1
    private var oldFullScore=-1
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        _binding = ScreenMainBinding.bind(view)
        loadButton()
        loadNum()
        init()
        viewModel.loadMat()
        viewModel.setScore()
        attachTouchListener()
        requireActivity().setStatusBar(binding.spase)
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                val dialog = requireActivity().createDialog(R.layout.dialog_back)
                dialog.findViewById<View>(R.id.home).setOnClickListener {
                    findNavController().popBackStack()
                    dialog.dismiss()
                }
                dialog.findViewById<View>(R.id.continue_btn).setOnClickListener {
                    dialog.dismiss()
                }
//                parentFragmentManager.popBackStack()
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)

    }

    private fun loadButton() {
        binding.apply {
            for (i in 0..<container.childCount) {
                var s = container[i] as LinearLayoutCompat
                for (j in 0..<s.childCount) {
                    buttons.add(s[j] as AppCompatTextView)
                }
            }

        }
    }

    private fun loadNum() {
        viewModel.load = { it, isFinish ->
            if (isFinish) {
                val dialog = FinishDialog()
                dialog.show(requireActivity().supportFragmentManager, "")
                dialog.onClickHome = {
                    findNavController().popBackStack()
                }
                dialog.onClickNewGame = {
                    viewModel.refresh()
                    viewModel.setScore()
                }
                dialog.isCancelable = false
                viewModel.cleatShared()
            }
            for (i in 0..<it.size) {
                for (j in 0..<it[i].size) {
                    if (it[i][j] == 0) {
                        buttons[i * 4 + j].text = ""
                    } else {
                        buttons[i * 4 + j].text = it[i][j].toString()
                    }
                    buttons[i * 4 + j].setBackgroundResource(getBackground(it[i][j]))
                }
            }
        }
        viewModel.showScore = {
            "showScore=> $it   $oldScore".myLog()
            binding.apply {
                if (oldScore >=it||oldScore==-1) {

                    currentScoreText.text = it.toString()
                } else {
                        "${it - oldScore}".myLog()
                        val currentScore = currentScoreText.text.toString().toInt()
                        animText(it - oldScore)
                        animNumber(currentScore, it)
                }
                var fullScore=fullScoreText.text.toString().toInt()
                if (fullScore < it&&it!=oldFullScore) {
                    if (oldFullScore!=-1){
                        animText2(it - oldFullScore)
                        animNumber2(oldFullScore,it)
                        oldFullScore=it
                    }else{
                        animText2(it-fullScore)
                        animNumber2(fullScore,it)
                        oldFullScore=it
                    }
                }
                oldScore = it
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun attachTouchListener() {
        val myTouchListener = MyTouchListener(requireContext())
        myTouchListener.setActionSideEnumListener {
            when (it) {
                MyTouchListener.SideEnum.DOWN -> viewModel.moveDown()
                MyTouchListener.SideEnum.RIGHT -> viewModel.moveRight()
                MyTouchListener.SideEnum.UP -> viewModel.moveUp()
                MyTouchListener.SideEnum.LEFT -> viewModel.moveLeft()
            }
            viewModel.loadMat()
            viewModel.setScore()
        }
        binding.background.setOnTouchListener(myTouchListener)
    }

    private fun getBackground(index: Int): Int {
        return when (index) {
            0 -> R.drawable.bg_item0
            2 -> R.drawable.bg_item2
            4 -> R.drawable.bg_item4
            8 -> R.drawable.bg_item8
            16 -> R.drawable.bg_item16
            32 -> R.drawable.bg_item32
            64 -> R.drawable.bg_item64
            128 -> R.drawable.bg_item128
            256 -> R.drawable.bg_item256
            512 -> R.drawable.bg_item512
            1024 -> R.drawable.bg_item1024
            else -> R.drawable.bg_item2048
        }
    }

    private fun init() {
        binding.apply {
            fullScoreText.text = shar.getHighScore().toString()
            home.setOnClickListener {
                val dialog = requireActivity().createDialog(R.layout.dialog_back, R.style.TransparentDialog)
                dialog.findViewById<View>(R.id.home).setOnClickListener {
                    findNavController().popBackStack()
                    dialog.dismiss()
                }
                dialog.findViewById<View>(R.id.continue_btn).setOnClickListener {
                    dialog.dismiss()
                }
            }
            refresh.setOnClickListener {
                val dialog = requireActivity().createDialog(R.layout.dialog_refresh)
                dialog.findViewById<View>(R.id.yes).setOnClickListener {
                    viewModel.refresh()
                    oldScore = 0
                    viewModel.setScore()
                    dialog.dismiss()
                }
                dialog.findViewById<View>(R.id.no).setOnClickListener {
                    dialog.dismiss()
                }

            }
        }
    }

    override fun onStop() {
        super.onStop()
        viewModel.setMat()
        shar.setHighScore(binding.fullScoreText.text.toString().toInt())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun animNumber(start: Int, end: Int) {
        val valueAnimator = ValueAnimator.ofInt(start, end)

        valueAnimator.duration = 500
        valueAnimator.addUpdateListener { animator ->
            val animatedValue = animator.animatedValue as Int
            binding.currentScoreText.text = "$animatedValue"
        }
        valueAnimator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)

            }
        })
        valueAnimator.start()
    }
    private fun animNumber2(start: Int, end: Int) {
        val valueAnimator = ValueAnimator.ofInt(start, end)

        valueAnimator.duration = 500

        valueAnimator.addUpdateListener { animator ->
            val animatedValue = animator.animatedValue as Int
            binding.fullScoreText.text = "$animatedValue"
        }
        valueAnimator.start()
    }

    @SuppressLint("SetTextI18n")
    fun animText(count: Int) = binding.apply {
        val scoreY = currentScore.y+currentScore.height/4
        val text = TextView(requireContext(),null,R.style.MyCustomTextStyle)
        text.text = "+$count"
        text.setTextColor(Color.YELLOW)
        text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24f)
//        text.textSize = 10.dpToPx().toFloat()
        val scoreX =currentScore.x+currentScore.width/2
        text.x = scoreX
        text.y = scoreY
        root.addView(text)
        text.post {
//            text.x=text.x-text.width/2
        }
        text.animate()
            .setDuration(800)
            .y(text.y - 64.dpToPx())
            .alpha(0.2f)
            .withEndAction {
                root.removeView(text)
            }
            .start()

    }
    private fun animText2(count: Int) = binding.apply {
        val scoreY = currentScore.y+currentScore.height/4
        val text = TextView(requireContext())
        text.text = "+$count"
        text.setTextColor(Color.YELLOW)
        text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24f)
        val scoreX =fullScore.x+fullScore.width/2-8.dpToPx()
        "scoreX=> $scoreX".myLog()
        text.x = scoreX
        text.y = scoreY
        root.addView(text)
        text.animate()
            .setDuration(800)
            .y(text.y - 64.dpToPx())
            .alpha(0.2f)
            .withEndAction {
                root.removeView(text)
            }
            .start()

    }

}