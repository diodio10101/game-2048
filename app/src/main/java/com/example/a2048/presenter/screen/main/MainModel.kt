package com.example.a2048.presenter.screen.main

import com.example.a2048.domain.AppRepository
import com.example.a2048.domain.AppRepositoryImpl

class MainModel:MainContract.Model {
    val appRepository:AppRepository=AppRepositoryImpl()
    override fun getMat(): ArrayList<ArrayList<Int>> =appRepository.getMat()
    override fun moveRight():Boolean =appRepository.moveRight()


    override fun moveLeft():Boolean=appRepository.moveLeft()


    override fun moveDown():Boolean =appRepository.moveDown()

    override fun moveUp():Boolean =appRepository.moveUp()


    override fun getScore(): Int =appRepository.getScore()
    override fun refresh(): ArrayList<ArrayList<Int>> {
        appRepository.refreshMatrix()
        return appRepository.getMat()
    }

    override fun setMat(score: Int) {
        appRepository.setMat(score)
    }

    override fun cleatShared() {
        appRepository.cleatShared()
    }


}