package com.example.a2048.presenter.screen.dialog

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import com.example.a2048.R
import com.example.a2048.databinding.DialogFinishBinding

class FinishDialog:DialogFragment() {
    var _binding:DialogFinishBinding?=null
    val binding by lazy { _binding!! }
    var onClickHome:(()->Unit)?=null
    var onClickNewGame:(()->Unit)?=null
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)

        // Настройка параметров окна диалога
        dialog.window?.let { window ->
            // Включение затемнения фона
            window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
            // Настройка уровня затемнения (от 0.0 до 1.0)
            window.setDimAmount(0.5f)
        }
        return dialog
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.TransparentDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding=DialogFinishBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.home.setOnClickListener{
            onClickHome?.invoke()
            dismiss()
        }
        binding.newGame.setOnClickListener {
            onClickNewGame?.invoke()
            dismiss()
        }
    }
    override fun onStart() {
        super.onStart()
        this.dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

}