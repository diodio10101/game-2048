package com.example.a2048.presenter.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.a20048.statusBarTRANSPARENT
import com.example.a2048.R
import com.example.a2048.presenter.screen.play.PlayScreen
import com.example.group1.utils.addScreen

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        statusBarTRANSPARENT()
    }
}