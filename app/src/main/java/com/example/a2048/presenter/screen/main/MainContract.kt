package com.example.a2048.presenter.screen.main

interface MainContract {
    interface Model{
        fun getMat():ArrayList<ArrayList<Int>>
        fun moveRight():Boolean
        fun moveLeft():Boolean
        fun moveDown():Boolean
        fun moveUp():Boolean
        fun getScore():Int
        fun refresh():ArrayList<ArrayList<Int>>
        fun setMat(score:Int)
        fun cleatShared()
    }
    interface ViewModel{
        fun setMat()
        fun loadMat()
        fun moveRight()
        fun moveLeft()
        fun moveDown()
        fun moveUp()
        fun setScore()
        fun refresh()
        fun cleatShared()
    }
}